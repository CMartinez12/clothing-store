/* Administrador */
//Crear Productos
// Create
function addProduct(){ // las BD hablan idiomas diferentes -> set, add hacen lo mismo
  let genderM = document.getElementById('genderProdM');
  let genderH = document.getElementById('genderProdH');
  let genderDef = "No definido";
  if(genderM.checked == true){
    genderDef = "Mujer";
  } else if(genderM.checked == true){
    genderDef = "Hombre";
  }
    db.collection("products").add( //Promete ingresar el producto
        {// crea el nuevo producto
          name: document.getElementById('nameProd').value,
          description: document.getElementById('descriptionProd').value,
          image:document.getElementById('imageProd').value,
          price: parseInt(document.getElementById('priceProd').value),
          gender: genderDef

        }
      ).then((docRef) => { //pude pude cumplir mi promesa
        console.log("Registro de producto exitoso!", docRef.id);
        document.getElementById('nameProd').value = "";
        document.getElementById('descriptionProd').value = "";
        document.getElementById('imageProd').value = "";
        document.getElementById('priceProd').value = "";
      }
      ).catch((err) => { // no pude ingresar el producto
        console.error("Error al insertar producto: ", err);
      });
}


// Ver Productos
// Read
function getProducts(){
    db.collection("products").onSnapshot((querySnapshot) => {
        tablaProd.innerHTML = '';
        console.log('PRODUCTOS OBTENIDOS');
        let counterProd = 0;
        querySnapshot.forEach((doc) => {
            let data = doc.data();
            console.log(`Producto ${doc.id} => ${data.name}`);
            tablaProd.innerHTML +=
            `
            <tr>
            <td> <img width="100px" src="${data.image}"></td>
            <td> ${data.name}</td>
            <td> ${data.description}</td>
            <td>$ ${data.price}</td>


         <td> <button type="button" name="button" class="btn btn-dark"  data-toggle="modal" data-target="#Modalactulizacionproducto${counterProd}">Actualizar</button> </td>
            <td> <button type="button" name="button" class="btn btn-danger"  onclick="deleteProduct('${doc.id}')">Eliminar</button> </td>
            </tr>

            <div class="modal fade" id="Modalactulizacionproducto${counterProd}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Actualizar Producto</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <div class="col">
                    <p>Nombre<br /><input type="text" id="newnameProd${counterProd}" placeholder="Name" value = "${data.name}">
                    </p>
                    <p>Descripcion<br /><textarea rows="4" cols="50"
                            id="newdescriptionProd${counterProd}" placeholder="Description" value = "${data.description}"></textarea>
                    </p>
                    <p>Imagen<br /><input type="text" id="newimageProd${counterProd}"
                            placeholder="https://url.com"></p>
                    <p> Para: <br />
                    <div class="maxl">
                        <label class="radio inline">
                            <input type="radio" name="gender" value="Hombre" checked
                                id="newgenderProd${counterProd}">
                            <span> Hombre </span>
                        </label>
                        <label class="radio inline">
                            <input type="radio" name="gender" value="Mujer"
                                id="newgenderProd${counterProd}">
                            <span> Mujer </span>
                        </label>
                    </div>
                    </p>
                    <p>Precio <br /><input type="text" id="newpriceProd${counterProd}"
                            placeholder="$0000" value = "${data.price}"></p>
                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" onclick="updateProduct('${doc.id}', '${counterProd}')" >Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
            `;
            counterProd +=1;
        });
    });
}

//Modificar Producto
// Update
function updateProduct(id, counterProd){
    console.log('=========================================>', document.getElementById('newnameProd'+counterProd).value);
    db.collection("products").doc(id).update(
        {
            name: document.getElementById('newnameProd'+counterProd).value,
            description: document.getElementById('newdescriptionProd'+counterProd).value,
            image:document.getElementById('newimageProd'+counterProd).value,
            price: parseInt(document.getElementById('newpriceProd'+counterProd).value),
            gender: document.getElementById('newgenderProd'+counterProd).value
        }
    ).then(() => {
        console.log('Modificación Exitosa');
        location.href = "PerfilAdministrador.html";
    })
    .catch((err) => {
        console.error("Error al hacer modificacion: ", err);
    });
}


// Borrar Productos
// Delete
function deleteProduct(id) {
    db.collection("products").doc(id).delete().then(()=>{
        console.log("El producto ha sido eliminado.");
    }).catch((err) => {
        console.log("Error al eliminar producto: ", err);
    });
}
