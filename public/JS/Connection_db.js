
// Your web app's Firebase configuration
let firebaseConfig = {
  apiKey: "AIzaSyCkf3fQbzvUIsXZXY9PXe0azaGC0X_c_nI",
  authDomain: "clothingstore-98789.firebaseapp.com",
  databaseURL: "https://clothingstore-98789.firebaseio.com",
  projectId: "clothingstore-98789",
  storageBucket: "clothingstore-98789.appspot.com",
  messagingSenderId: "57840967",
  appId: "1:57840967:web:635dbd4161ca5d909e6862",
  measurementId: "G-KMCHVZ9J10"
};


// Initialize Firebase
firebase.initializeApp(firebaseConfig);
let db = firebase.firestore();
console.log('Base de datos conectada!');

// Registro de Usuario y Administradores
// Create
function addUser(type) {

  let name = 'name';
  let lastname = 'lastname';
  let email = 'email';
  let born = 'born';
  let gender = 'gender';
  let password = 'password';
  let confpassword = 'confpassword';

  if (type == 'Usuario') {
    name = 'uname';
    lastname = 'ulastname';
    email = 'uemail';
    born = 'uborn';
    gender = 'ugender';
    password = 'upassword';
    confpassword = 'uconfpassword';
  }

  let confPassV = document.getElementById(confpassword).value;
  let passwordV = document.getElementById(password).value;

  if (confPassV == passwordV) {
    db.collection("users").add(
      {
        name: document.getElementById(name).value,
        lastname: document.getElementById(lastname).value,
        email: document.getElementById(email).value,
        gender: document.getElementById(gender).value,
        born: document.getElementById(born).value,
        type: type,
        password: passwordV
      }
    ).then((docRef) => {
      console.log("Registro exitoso!", docRef.id);
      document.getElementById(name).value = "";
      document.getElementById(lastname).value = "";
      document.getElementById(email).value = "";
      document.getElementById(gender).value = "";
      document.getElementById(born).value = "";
      document.getElementById(password).value = "";
      document.getElementById(confpassword).value = "";
      location.href = "../index.html";
    }
    ).catch((err) => {
      console.error("Error al insertar usuario: ", err);
    });
  } else {
    alert("Confimacion de la contraseña es diferente a contraseña!")
  }

}

// Login de Usuarios y Administradores
function login() {

  let email = document.getElementById("emailLog").value;
  let password = document.getElementById("passwordLog").value;

  let user = db.collection("users").where("email", "==", email)
    .get().then((query) => {  //{Mateo, Daisy}
      query.forEach((doc) => { // Mateo
        ///
        console.log("Obtuvo Usuario");
        let data = doc.data();
        if (data.password == password) {
          localStorage.setItem("user", doc.id);
          if (data.type == 'Administrador') {
            location.href = "Administrador/PerfilAdministrador.html";
          } else {
            location.href = "Usuarios/PerfilUsuario.html";
          }
        } else {
          alert('Email and Password incorrects');
        }

        ///
      })
    })
    .catch((err) => {
      console.error('Error al hacer el login: ', err);
    });
}

//Ver Usuario
function showUser(id) {
  db.collection("users").doc(id).get().then((doc) => {
    if (doc.exists) {
      let data = doc.data();
      // Ver id de html
      nameUser.innerHTML = `<h5>${data.name} ${data.lastname}</h5>`
      
      infoUser.innerHTML = `
        <div class="row">
                               <div class="col-md-6">
                                    <label>Identificación en DB</label>
                                </div>
                                <div class="col-md-6">
                                    <p id="identificacion">${id}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Nombre</label>
                                </div>
                                <div class="col-md-6">
                                    <p id="name">${data.name} ${data.lastname}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email:</label>
                                </div>
                                <div class="col-md-6">
                                    <p id="email">${data.email}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Fecha de Nacimiento</label>
                                </div>
                                <div class="col-md-6">
                                    <p id = "born">${data.born}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Genero</label>
                                </div>
                                <div class="col-md-6">
                                    <p id="gender">${data.gender}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Tipo de Usuario</label>
                                </div>
                                <div class="col-md-6">
                                    <p id="type">${data.type}</p>
                                </div>
                            </div>
                            `

    }
  }).catch((err) => {
    console.error('Error al ver el usuario: ', err);
  });
}

// Cambiar Datos de Usuario y Administrador
function updateUser(id) {
  let password = document.getElementById('actpassword').value;
  let email = document.getElementById('actemail').value;

  let user = db.collection("users").doc(id)
    .get().then((query) => {
      let data = query.data();
      if (data.password == password) {
        let newpassword = document.getElementById('actnewpassword').value;
        let confPass = document.getElementById('actconfpassword').value;

        if (confPass == newpassword) {
          db.collection("users").doc(id).update(
            {
              name: document.getElementById('actname').value,
              lastname: document.getElementById('actlastname').value,
              email: email,
              gender: document.getElementById('actgender').value,
              born: document.getElementById('actborn').value,
              password: newpassword
            }
          ).then(() => {
            console.log('Modificación Exitosa');
            if (data.type == 'Administrador'){
              location.href = "PerfilAdministrador.html"
            }else{
              location.href = "PerfilUsuario.html"
            }
            
          })
            .catch((err) => {
              console.error("Error al hacer modificacion: ", err);
            });
        } else {
          alert('La nueva contraseña no e igual a su confirmacion!')
        }
      }
    }).catch((err) => {
      console.error('Error al hacer el login: ', err);
    });

}
