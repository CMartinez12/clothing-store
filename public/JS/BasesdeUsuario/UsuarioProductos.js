// Ver Productos
// Read

// Requeriiento Adicional: Ver Productos desde la Base de Datos filtrados por el typo de usuario (mujeres, hombres)

function getUserProductsHombre(){
  hombresDIV.innerHTML = '';
        let iterator = 0;
    db.collection("products").where("gender","==","Hombre").get().then((querySnapshot) => {
        //console.log('PRODUCTOS OBTENIDOS');
        querySnapshot.forEach((doc) => {
            let data = doc.data();
            //console.log(`Producto ${doc.id} => ${data.name}`);
            hombresDIV.innerHTML +=
            `
            <div class="col-md-4" data-toggle="modal" data-target="#modal${iterator}">
              <div class="card mb-4 shadow-sm">
                <img src="${data.image}" class="bd-placeholder-img card-img-top" height="225" alt="${data.name}" />
                <div class="card-body">
                <i style="color:orange;" class="fa fa-star"></i>
                <i style="color:orange;" class="fa fa-star"></i>
                <i style="color:orange;" class="fa fa-star"></i>
                <i style="color:orange;" class="fa fa-star"></i>
                <i style="color:orange;" class="fa fa-star"></i>
                  <p class="card-text"> <b>${data.name}</b> <br/> ${data.description}</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary" OnClick=location.href='Vistas/cart.html'>Comprar</button>
                      <button id="btn${iterator}" type="button" class="btn btn-sm btn-outline-secondary" OnClick="addCartItem('${doc.id}', 'btn${iterator}');">Añadir al carrito</button>
                    </div>
                    <small class="text-muted">$ ${data.price}</small>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="modal${iterator}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
              <div class="modal-content mb-4 shadow-sm">
                <img src="${data.image}" class="bd-placeholder-img card-img-top" height="225" alt="${data.name}" />
                <div class="card-body">
                <i style="color:orange;" class="fa fa-star"></i>
                <i style="color:orange;" class="fa fa-star"></i>
                <i style="color:orange;" class="fa fa-star"></i>
                <i style="color:orange;" class="fa fa-star"></i>
                <i style="color:orange;" class="fa fa-star"></i>
                  <p class="card-text"> <b>${data.name}</b> <br/> ${data.description}</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary" OnClick=location.href='Vistas/cart.html'>Comprar</button>
                    </div>
                    <small class="text-muted">$ ${data.price}</small>
                  </div>
                </div>
              </div>
              </div>
            </div>
            `;
            iterator +=1;
        })}).catch((err)  => {
          console.error('Error Producto No Encontrado: ', err);
        });
}


function getUserProductsMujer(){
  mujeresDIV.innerHTML = '';
  let iterator = 0;
db.collection("products").where("gender","==","Mujer").get().then((querySnapshot) => {
  //console.log('PRODUCTOS OBTENIDOS');
  querySnapshot.forEach((doc) => {
      let data = doc.data();
      //console.log(`Producto ${doc.id} => ${data.name}`);
      mujeresDIV.innerHTML +=
      `
      <div class="col-md-4" data-toggle="modal" data-target="#modalM${iterator}">
        <div class="card mb-4 shadow-sm">
          <img src="${data.image}" class="bd-placeholder-img card-img-top" height="225" alt="${data.name}" />
          <div class="card-body">
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
            <p class="card-text"> <b>${data.name}</b> <br/> ${data.description}</p>
            <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group">
                <button type="button" class="btn btn-sm btn-outline-secondary" OnClick=location.href='Vistas/cart.html'>Comprar</button>
                <button id="btn${iterator}" type="button" class="btn btn-sm btn-outline-secondary" OnClick="addCartItem('${doc.id}', 'btn${iterator}');">Añadir al carrito</button>
              </div>
              <small class="text-muted">$ ${data.price}</small>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="modalM${iterator}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content mb-4 shadow-sm">
          <img src="${data.image}" class="bd-placeholder-img card-img-top" height="225" alt="${data.name}" />
          <div class="card-body">
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
            <p class="card-text"> <b>${data.name}</b> <br/> ${data.description}</p>
            <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group">
                <button type="button" class="btn btn-sm btn-outline-secondary" OnClick=location.href='Vistas/cart.html'>Comprar</button>
              </div>
              <small class="text-muted">$ ${data.price}</small>
            </div>
          </div>
        </div>
        </div>
      </div>
      `;
      iterator +=1;
  })}).catch((err)=>{
    console.error('Error Producto No Encontrado: ', err);
  });
}

function principalSelect(nombre) {
  var hombres = document.getElementById("hombresDIV");
  var mujeres = document.getElementById("mujeresDIV");
  var local = document.getElementById("localstoreDIV");
  var hrhombre = document.getElementById("hr-hombres");
  var hrmujer = document.getElementById("hr-mujeres");
  var hrlocal = document.getElementById("hr-local");
  var titlehombres = document.getElementById("hombres");
  var titlemujeres = document.getElementById("mujeres");
  var titlelocal = document.getElementById("local store");
  if (nombre == "hombresDIV") {
    hombres.style.display = "flex";
    hrhombre.style.display = "flex";
    titlehombres.style.display = "flex";
    mujeres.style.display = "none";
    hrmujer.style.display = "none";
    titlemujeres.style.display = "none";
    local.style.display = "none";
    hrlocal.style.display = "none";
    titlelocal.style.display = "none";
  } else if (nombre == "mujeresDIV") {
    hombres.style.display = "none";
    hrhombre.style.display = "none";
    titlehombres.style.display = "none";
    mujeres.style.display = "flex";
    hrmujer.style.display = "flex";
    titlemujeres.style.display = "flex";
    local.style.display = "none";
    hrlocal.style.display = "none";
    titlelocal.style.display = "none";
  } else if (nombre == "localstoreDIV") {
    hombres.style.display = "none";
    hrhombre.style.display = "none";
    titlehombres.style.display = "none";
    mujeres.style.display = "none";
    hrmujer.style.display = "none";
    titlemujeres.style.display = "none";
    local.style.display = "flex";
    hrlocal.style.display = "flex";
    titlelocal.style.display = "flex";
  }
}

$("#nav-hombres").click(function () {
  document.getElementById("container-marketing").style.display = "block";
  principalSelect("hombresDIV");
});

$("#nav-mujeres").click(function () {
  document.getElementById("container-marketing").style.display = "block";
  principalSelect("mujeresDIV");
});

$("#nav-local").click(function () {
  document.getElementById("container-marketing").style.display = "block";
  principalSelect("localstoreDIV");
});

$("#nav-home").click(function () {
  document.getElementById("container-marketing").style.display = "none";
});

//Buscador de productos
function searchProduct(){
  let nombre = document.getElementById('searchProd').value;
  resultSearch.innerHTML = '';
  let iterator = 0;
db.collection("products").where("name","==",nombre).get().then((querySnapshot) => {
  //console.log('PRODUCTOS OBTENIDOS');
  querySnapshot.forEach((doc) => {
      let data = doc.data();
      //console.log(`Producto ${doc.id} => ${data.name}`);
      resultSearch.innerHTML +=
      `
      <div>
        <div class="card mb-4 shadow-sm">
          <img src="${data.image}" class="bd-placeholder-img card-img-top"  style="{width:50%}" alt="${data.name}" />
          <div class="card-body">
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
          <i style="color:orange;" class="fa fa-star"></i>
            <p class="card-text"> <b>${data.name}</b></p>
            <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group">
                <button type="button" class="btn btn-sm btn-outline-secondary" OnClick=location.href='Vistas/cart.html'>Comprar</button>
                <button id="btn${iterator}" type="button" class="btn btn-sm btn-outline-secondary" OnClick="addCartItem('${doc.id}', 'btn${iterator}');">Añadir al carrito</button>
              </div>
              <small class="text-muted">$ ${data.price}</small>
            </div>
          </div>
        </div>
      </div>
      `;
      iterator +=1;
  })}).catch((err)=>{
    console.error('Error Producto No Encontrado: ', err);
  });
}


//Agregar Productos al Carrito de la compra
function animation() {
  const toast = Swal.fire({
    //type: "success",
    title: "Added to shopping cart",
    toast: true,
    position: "top-end",
    timer: 2000,
    showConfirmButton: false,
  });
}

var cartItems = [];
var products = [];

function addCartItem(idProduct, btnProd){
  console.log('Producto agregado', idProduct);
  cartItems.push(idProduct);
  let storage = JSON.parse(localStorage.getItem("cart"));
  if (storage == null) {
    products.push(idProduct);
    localStorage.setItem("cart", JSON.stringify(products));
    sessionStorage.setItem("cart", JSON.stringify(products));
  } else {
    products = JSON.parse(localStorage.getItem("cart"));
    products.push(idProduct);
    localStorage.setItem("cart", JSON.stringify(products));
    sessionStorage.setItem("cart", JSON.stringify(products));
  }
  products = JSON.parse(localStorage.getItem("cart"));
  cart_n.innerHTML = `[${products.length}]`;
  document.getElementById(btnProd).style.display = "none";
  animation();
}

function removeCartItems(){
  localStorage.clear();
  sessionStorage.clear();
  showCartItems();
  location.href = "cart.html";
}


// Ver datos en localStorage
function showCartItems(){
  let cartProducts = JSON.parse(localStorage.getItem("cart"));
  if (cartProducts != null){
    for (let i = 0; i < cartProducts.length; i++){
      let docRef = db.collection("products").doc(cartProducts[i]);
      docRef.get().then((doc) => {
          if (doc.exists) {
              let data = doc.data();
              //console.log("Document data ==>", data.description);
              tablaProdsCart.innerHTML +=
              `
              <tr>
              <td> <img width="100px" src="${data.image}"></td>
              <td> ${data.name}</td>
              <td> ${data.description}</td>
              <td>$ ${data.price}</td>

              <td> <button type="button" name="button" class="btn btn-danger" onclick="removeCartProduct('${cartProducts[i]}')">Quitar</button> </td>
              </tr>
              `;
          } else {
              console.log("No such document!");
          }
      }).catch(function(error) {
          console.log("Error getting document:", error);
      });
    }
  }
}

function removeCartProduct(idProduct){
  let cartProducts = JSON.parse(localStorage.getItem("cart"));
  let newCartProducts = []
  for (let i = 0; i < cartProducts.length; i++){
    if (cartProducts[i] != idProduct){
      newCartProducts.push(cartProducts[i]);
    }
  }
  localStorage.setItem("cart", JSON.stringify(newCartProducts));
  tablaProdsCart.innerHTML = '';
  showCartItems();
}




// Ver Carrito de la compra
function createOrder(){
  db.collection("products").add( //Promete ingresar el producto
      {// crea el nuevo producto
        name: document.getElementById('nameProd').value,
        description: document.getElementById('descriptionProd').value,
        image:document.getElementById('imageProd').value,
        price: parseInt(document.getElementById('priceProd').value)
      }
    ).then((docRef) => { //pude pude cumplir mi promesa
      console.log("Registro de producto exitoso!", docRef.id);
      document.getElementById('nameProd').value = "";
      document.getElementById('descriptionProd').value = "";
      document.getElementById('imageProd').value = "";
      document.getElementById('priceProd').value = "";
    }
    ).catch((err) => { // no pude ingresar el producto
      console.error("Error al insertar producto: ", err);
    });
}
