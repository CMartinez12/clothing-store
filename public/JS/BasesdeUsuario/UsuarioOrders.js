function createOrder(){
  let user = localStorage.getItem("user"); // JSON.parse(localStorage.getItem("cart"));
  if (user == null) {
    location.href = "https://www.deepl.com/translator#en/es/whenfurther";
  } else {
    let cartItems = localStorage.getItem("cart");
    let currentDate = new Date();
    db.collection("orders").add(
      {
        user: user,
        products: cartItems,
        date: currentDate
      }
    ).then((docRef) => {
      console.log("Orden creada!", docRef.id);
      removeCartItems();
    }
    ).catch((err) => {
      console.error("Error al crear la orden:  ", err);
    });
  }
}

function showOrders() {
  db.collection("orders").onSnapshot((querySnapshot) => {
      tablaOrders.innerHTML = '';
      //console.log('PRODUCTOS OBTENIDOS');
      querySnapshot.forEach((doc) => {
          let data = doc.data();
          //console.log(`Producto ${doc.id} => ${data.name}`);
          tablaOrders.innerHTML +=
          `
          <tr>
          <td> ${doc.id} ></td>
          <td> ${data.date}</td>
          <td> <button type="button" name="button" class="btn btn-dark" onclick="showOrder('${doc.id}')">Ver Orden</button> </td>
          </tr>
          `;
      });
  });
}
showOrders();


function showProduct(idProduct) {
  orderInfo.innerHTML= "";
  let docRef = db.collection("Products").doc(idOrder);
  docRef.get().then((doc) => {
      if (doc.exists) {
          return doc.data();
      } else {
          console.log("No such document!");
      }
  }).catch(function(error) {
      console.log("Error getting document:", error);
  });
}

function showOrder(idOrder) {
  orderInfo.innerHTML= "";
  let docRef = db.collection("orders").doc(idOrder);
  docRef.get().then((doc) => {
      if (doc.exists) {
          let data = doc.data();
          //console.log("Document data ==>", data.description);
          let productsOrder = data.products;
          let cost = 0;
          for (let i = 0; i<productsOrder.length; i++){
            let prd = showProduct(productsOrder[i]);
            let prdPrice = prd.price;
            orderInfo.innerHTML +=
            `<hr>
            <p>
              <br/> Nombre: ${prd.name}
              <br/> Descripcion: ${prd.description}
              <br/> Precio: ${prdPrice}
            </p>
            `;
            cost+=prdPrice;
          }
          orderInfo.innerHTML += `<b>Costo ${cost}</b>`
      } else {
          console.log("No such document!");
      }
  }).catch(function(error) {
      console.log("Error getting document:", error);
  });
}

showOrder();
