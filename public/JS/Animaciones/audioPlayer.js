function audioPlayer() {
    var currentSong = 0;
    $("#audioPlayer")[0].src = $("#playlist li a")[0];

    var promise = document.querySelector('#audioPlayer').play();

if (promise !== undefined) {
  promise.then(_ => {
    // Autoplay started!
  }).catch(error => {
    // Autoplay was prevented.
    // Show a "Play" button so that user can start playback.
  });
}

    
    $("#playlist li a").click(function (e) {
        e.preventDefault();
        $("#audioPlayer")[0].src = this;
        $("#audioPlayer")[0].play();
        $("#playlist li").removeClass("current-song");
        currentSong = $(this).parent().index();
        $(this).parent().addClass("current-song");
    });
    document.querySelector('#audioPlayer').addEventListener("ended", function(){
        currentSong++;
        if(currentSong== $("playlist li a").length)
        currentSong = 0;
        $("playlist li").removeClass("current-song");
        $("playlist li:eq("+currentSong+")").addClass("current-song");
        $("audioPlayer")[0].src=$("#playlist li a")[currentSong].href;
        $("audioPlayer")[0].play();

    })
}